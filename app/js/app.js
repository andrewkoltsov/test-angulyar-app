'use strict';

/* App Module */

angular.module('vtt', []).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/home', {templateUrl: 'partials/home.html',   controller: HomeCtrl}).
      when('/form', {templateUrl: 'partials/form.html', controller: FormCtrl}).
      otherwise({redirectTo: '/home'});
}]);
