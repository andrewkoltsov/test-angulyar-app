'use strict';

/* Controllers */

function HomeCtrl($scope) {
  
}

//HomeCtrl.$inject = ['$scope'];


function FormCtrl($scope, $routeParams) {
	console.log($routeParams);
	$scope.form1_input1 = $routeParams['input1'] || "" ;
	$scope.result_rows = [];

	$scope.form1_query_data = function() {
		$scope.form1_input1_data = $scope.form1_input1; //funcking live binding
		$("#form1_results").dataTable().fnReloadAjax("data/data.json");
	};
	$("#form1_results").dataTable({
		"bProcessing": true,
        "bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "200px",
        "bServerSide": true,
        // "sAjaxSource": "data/data.json",
        "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "input1", "value": $scope.form1_input1_data } );
			if (sSource) {
				$.getJSON( sSource, aoData, function (json) { 
					/* Do whatever additional processing you want on the callback, then tell DataTables */
					fnCallback(json);
				} );
			} else {
				fnCallback({
					"aaData":[],
					"iTotalRecords": "0",
    				"iTotalDisplayRecords": "0"
    			});
			}	
		}
        
	});
}

//FormCtrl.$inject = ['$scope', '$routeParams'];
